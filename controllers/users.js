const express = require('express');



function list(req, res, next) {
  res.send('Users in system');
}

function index(req, res, next) {
  res.send(`User with ID ${req.params.id}`);
}

function create(req, res, next) {
  const name = req.body.name;
  const lastName = req.body.lastName;

  res.send(`Create new user with first and last name ${name} ${lastName}`);
}

function replace(req, res, next) {
  res.send(`replace user with id ${req.params.id} with other`);
}

function edit(req, res, next) {
  res.send(`Changed properties of user with ID ${req.params.id}`);
}

function destroy(req, res, next) {
  res.send(`User with ID ${req.params.id}was eliminated`);
}

module.exports = {
  list, index, create, replace, edit, destroy
}
