FROM node
MAINTAINER Juan Pablo Martinez Cantu
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start
